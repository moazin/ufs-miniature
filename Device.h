#ifndef DEVICE_H
#define DEVICE_H

#include <cstdint>

class Device
{
  private:
    uint8_t *bytes;
    uint32_t total_blocks;
  public:
    const uint32_t BLOCK_SIZE = 4096;
    Device(uint32_t total_blocks);
    ~Device();
    void readBlock(uint32_t index, uint8_t *write_location);
    void writeBlock(uint32_t index, uint8_t *read_location);
    uint32_t getTotalBlocks();
};
#endif
