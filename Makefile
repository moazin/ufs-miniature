all:
	g++ -c -g -O0 Device.cpp -o build/Device.o
	g++ -c -g -O0 UFS.cpp -o build/UFS.o
	g++ -c -g -O0 main.cpp -o build/main.o
	g++ -g -O0 build/main.o build/Device.o build/UFS.o -o build/main
