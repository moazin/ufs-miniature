#include "Device.h"

Device::Device(uint32_t total_blocks)
{
  this->bytes = new uint8_t[total_blocks * this->BLOCK_SIZE];
  this->total_blocks = total_blocks;
}

Device::~Device()
{
  delete[] this->bytes;
}

void Device::readBlock(uint32_t block_no, uint8_t *write_location)
{
  for(uint32_t i = 0; i < BLOCK_SIZE; i++)
    write_location[i] = this->bytes[i + BLOCK_SIZE*block_no];
}

void Device::writeBlock(uint32_t block_no, uint8_t *read_location)
{
  for(uint32_t i = 0; i < BLOCK_SIZE; i++)
    this->bytes[i + BLOCK_SIZE*block_no] = read_location[i];
}

uint32_t Device::getTotalBlocks()
{
  return this->total_blocks;
}
