#include <iostream>
#include "Device.h"
#include "UFS.h"
#include <cstdint>
#include <cstring>

void printData(UFS *ufs, File *file)
{
  uint8_t *blob = new uint8_t[file->getSize()];
  ufs->readFromFile(file->getInodeIndex(), blob);
  for(int i = 0; i < file->getSize(); i++)
  {
    std::cout << (char)blob[i];
  }
  std::cout << std::endl;
  delete[] blob;
}

void writeABC(UFS *ufs, File *file, int ammount, int offset)
{
  uint8_t *blob = new uint8_t[ammount];
  char c = 'A';
  for(int i = 0; i < ammount; i++)
  {
    blob[i] = c;
    if(c > 'z') c = 'A';
    else c += offset;
  }
  ufs->writeFile(file, ammount, blob);
  delete[] blob;
}

int main(void)
{
  Device usb(3000);

  UFS ufs(&usb);

  ufs.format();

  Directory *root = ufs.getRoot();

  Directory *directory = ufs.createDirectory((Entity*)root, "even");
  Directory *directory2 = ufs.createDirectory((Entity*)root, "odd");

  File* range1e  = ufs.createFile(directory, "range1.txt");
  File* range2e  = ufs.createFile(directory, "range2.txt");
  File* range3e  = ufs.createFile(directory, "range3.txt");

  File* range1o = ufs.createFile(directory2, "range1.txt");
  File* range2o = ufs.createFile(directory2, "range2.txt");
  File* range3o = ufs.createFile(directory2, "range3.txt");

  ufs.printDirectory(0, root);

  writeABC(&ufs, range1e, 10000, 2);

  printData(&ufs, range1e);

  ufs.deleteFile(range1e);
  ufs.printDirectory(0, root);
  return 0;
}
