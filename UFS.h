#ifndef UFS_H_
#define UFS_H_

#include <string>
#include <vector>
#include "Device.h"

class Entity
{
  protected:
    std::string name;
    uint32_t inode_index;
  public:
    enum Type
    {
      File,
      Dir
    };
    virtual std::string getName() = 0;
    virtual Type getType() = 0;
    virtual uint32_t getInodeIndex();
};

class Directory: public Entity
{
  private:
    std::string name;
    uint32_t files_count;
    Directory* parent;
  public:
    Directory(std::string name, Directory *parent, uint32_t files_count, uint32_t inode_index);
    std::string getName();
    uint32_t getFilesCount();
    Type getType();
    Directory* getParent();
};

class File: public Entity
{
  private:
    uint32_t size;
    Directory* parent;
  public:
    File(std::string name, Directory* parent, uint32_t size, uint32_t inode_index);
    std::string getName();
    uint32_t getSize();
    void setSize(uint32_t size);
    Type getType();
    Directory* getParent();
};

struct Superblock
{
  uint32_t state;
  uint32_t file_system_size;
  uint32_t number_free_blocks;
  uint32_t pointer_free_block;
  uint32_t index_next_free_block;
  uint32_t size_inode_list;
  uint32_t number_free_inodes;
  uint32_t index_next_free_inode;
};

struct Inode
{
  uint32_t type; // 0 -> file; 1 -> directory
  uint32_t size;
  uint32_t data[12];
  uint32_t next_inode;
};

class UFS
{
  private:
    Device* device;
    const uint32_t INODE_SIZE = 1000;
  public:
    UFS(Device* device);
    void format();
    Directory* getRoot();
    std::vector<Entity*> dir(Entity* parent);
    void erase(Entity* entity);
    File* createFile(Entity* parent, std::string name);
    Directory* createDirectory(Entity* parent, std::string name);
    void writeFile(File* file, uint32_t size, uint8_t *bytes);
    void readFile(File* file, uint8_t *bytes);
    void deleteFile(File* file);

    // sorta internal functions
    Superblock getSuperblock();
    void setSuperblock(Superblock block);
    void setInode(uint32_t index, Inode inode);
    Inode getInode(uint32_t index);

    uint32_t requestMemoryBlock();
    uint32_t freeMemoryBlock(uint32_t index);

    uint32_t requestInode();
    void freeInode(uint32_t index);

    void writeToFile(uint32_t inode_index, uint8_t *contents, uint32_t size);
    void readFromFile(uint32_t inode_index, uint8_t *contents);

    uint32_t file_list_to_string(std::vector<Entity*> entities, uint8_t **location);
    void string_to_file_list(uint8_t *loc, Directory* parent, std::vector<Entity*> &entities);

    void dir(Directory *parent, std::vector<Entity*> &entities);
    void eraseFile(uint32_t inode_index);
    uint32_t getFilesCountOfInode(uint32_t inode_index);

    void printFile(int tab, File* file);
    void printDirectory(int tab, Directory *root);

};


#endif
