#include "UFS.h"
#include <iostream>
#include <cmath>
#include <cstring>

uint32_t Entity::getInodeIndex()
{
  return this->inode_index;
}


void File::setSize(uint32_t size)
{
  this->size = size;
}

File::File(std::string name, Directory* parent, uint32_t size, uint32_t inode_index)
{
  this->name = name;
  this->parent = parent;
  this->size = size;
  this->inode_index = inode_index;
}

std::string File::getName()
{
  return this->name;
}

uint32_t File::getSize()
{
  return this->size;
}

File::Type File::getType()
{
  return File::Type::File;
}

Directory* File::getParent()
{
  return this->parent;
}

Directory::Directory(std::string name, Directory* parent, uint32_t files_count, uint32_t inode_index)
{
  this->name = name;
  this->parent = parent;
  this->files_count = files_count;
  this->inode_index = inode_index;
}

Directory* Directory::getParent()
{
  return this->parent;
}

std::string Directory::getName()
{
  return this->name;
}

uint32_t Directory::getFilesCount()
{
  return this->files_count;
}

Directory::Type Directory::getType()
{
  return Directory::Type::Dir;
}


UFS::UFS(Device* device)
{
  this->device = device;
}

Superblock UFS::getSuperblock()
{
  uint8_t tmp[4096];

  this->device->readBlock(0, tmp);

  Superblock *superblock = (Superblock*)tmp;

  return *superblock;
}

void UFS::setSuperblock(Superblock superblock)
{
  uint8_t tmp[4096];

  Superblock *block = (Superblock*)tmp;
  *block = superblock;

  this->device->writeBlock(0, tmp);
}

void UFS::format()
{
  // perform necessary calculations
  const uint16_t inodes_per_block = this->device->BLOCK_SIZE / sizeof(Inode);
  const uint16_t inode_blocks_needed = ceil(((float)this->INODE_SIZE) / ((float)inodes_per_block));

  const uint16_t blocks_left = this->device->getTotalBlocks() - inode_blocks_needed - 1;
  const uint16_t really_free_blocks = 1023*(blocks_left - 1)/1024 + 1;

  // the number of blocks that'll keep track of free blocks
  const uint16_t free_record_blocks = blocks_left - really_free_blocks;

  const uint32_t inode_start_index = 1;
  const uint32_t inode_end_index = inode_blocks_needed; // this is the last valid inode block

  const uint32_t free_list_start_index = inode_end_index + 1;
  const uint32_t free_list_end_index = free_list_start_index + free_record_blocks - 1;
  const uint32_t free_blocks_start_index = free_list_end_index + 1;
  const uint32_t free_blocks_end_index = this->device->getTotalBlocks() - 1;

  // mark the freed blocks and create their linked list
  uint32_t free_block_index = free_blocks_end_index;
  int32_t entry_index = 1023;
  uint32_t last_pointer = 0;
  uint32_t block_index;
  for(block_index = free_list_end_index; block_index >= free_list_start_index; block_index--)
  {
    uint8_t tmp_block[4096];
    uint32_t *loc;
    entry_index = 1022;
    while(entry_index >= 0 && free_block_index >= free_blocks_start_index)
    {
      loc = (uint32_t*)(tmp_block + (4 + entry_index*4));
      *loc = free_block_index--;
      entry_index--;
    }
    loc = (uint32_t*)(tmp_block);
    *loc = last_pointer;

    this->device->writeBlock(block_index, tmp_block);
    last_pointer = block_index;
  }
  block_index++;
  entry_index++;

  // write the info
  Superblock block;

  block.state = 0;
  block.file_system_size = this->device->getTotalBlocks();
  block.number_free_blocks = free_blocks_end_index - free_blocks_start_index + 1;
  block.pointer_free_block = block_index;
  block.index_next_free_block = entry_index;
  block.size_inode_list = this->INODE_SIZE;
  block.number_free_inodes = block.size_inode_list - 1; // since we are already initing a root directory
  block.index_next_free_inode = 1;

  uint32_t next_pointer = 0;
  for(uint32_t i = this->INODE_SIZE-1; i >= 1; i--)
  {
    Inode inode;
    inode.next_inode = next_pointer;
    this->setInode(i, inode);
    next_pointer = i;
  }

  Inode inode;
  inode.type = 1;
  inode.size = 0;
  this->setInode(0, inode);

  this->setSuperblock(block);

}

void UFS::setInode(uint32_t index, Inode inode_in)
{
  const uint32_t base = 1; // since inodes start at 1
  const uint32_t inodes_per_block = (this->device->BLOCK_SIZE / sizeof(Inode));
  uint32_t block_offset = index / inodes_per_block;
  uint32_t index_in_block = index % inodes_per_block;
  uint8_t data[4096];
  this->device->readBlock(block_offset + base, data);

  Inode* inode = (Inode*)data;
  inode[index_in_block] = inode_in;

  this->device->writeBlock(block_offset + base, data);
}

Inode UFS::getInode(uint32_t index)
{
  const uint32_t base = 1; // since inodes start at 1
  const uint32_t inodes_per_block = (this->device->BLOCK_SIZE / sizeof(Inode));
  uint32_t block_offset = index / inodes_per_block;
  uint32_t index_in_block = index % inodes_per_block;
  uint8_t data[4096];
  this->device->readBlock(block_offset + base, data);

  Inode* inode = (Inode*)data;
  return inode[index_in_block];
}

uint32_t UFS::requestMemoryBlock()
{
  Superblock block = this->getSuperblock();
  if (block.number_free_blocks == 0)
    return 0;

  uint32_t list_index = block.pointer_free_block;
  uint32_t index = block.index_next_free_block;

  uint8_t data[4096];
  this->device->readBlock(list_index, data);

  uint32_t *block_to_occupy = (uint32_t*)(data + 4 + 4*index);

  block.number_free_blocks--;
  if(index == 1022)
  {
    uint32_t *next_list_block = (uint32_t*)(data);
    block.index_next_free_block = 0;
    block.pointer_free_block = *next_list_block;
    this->setSuperblock(block);
  }
  else
  {
    block.index_next_free_block++;
    this->setSuperblock(block);
  }
  return *block_to_occupy;
}

uint32_t UFS::freeMemoryBlock(uint32_t index_in)
{
  Superblock block = this->getSuperblock();

  uint32_t list_index = block.pointer_free_block;
  uint32_t index = block.index_next_free_block;

  uint8_t data[4096];
  if(index == 0)
  {
    uint32_t new_block = list_index - 1;
    uint32_t* loc = (uint32_t*)data;
    *loc = list_index;
    loc = (uint32_t*)(data + 4 + (1022*4));
    *loc = index_in;
    block.number_free_blocks++;
    block.index_next_free_block = 1022;
    block.pointer_free_block = new_block;
    this->device->writeBlock(new_block, data);
    this->setSuperblock(block);
    return index_in;
  }
  else
  {
    this->device->readBlock(list_index, data);
    uint32_t* block_number = (uint32_t*)(data + 4 + (index - 1)*4);
    *block_number = index_in;
    this->device->writeBlock(list_index, data);
    block.number_free_blocks++;
    block.index_next_free_block--;
    this->setSuperblock(block);
    return index_in;
  }
}

uint32_t UFS::requestInode()
{
  Superblock block = this->getSuperblock();
  if(block.number_free_blocks == 0)
    return 0;

  uint32_t inode_to_return = block.index_next_free_inode;
  Inode inode = this->getInode(inode_to_return);
  block.index_next_free_inode = inode.next_inode;
  block.number_free_inodes--;
  this->setSuperblock(block);
  return inode_to_return;
}

void UFS::freeInode(uint32_t index)
{
  Superblock block = this->getSuperblock();

  uint32_t current_free = block.index_next_free_inode;
  Inode inode = this->getInode(index);
  inode.next_inode = current_free;
  this->setInode(index, inode);
  block.index_next_free_inode = index;
  block.number_free_inodes++;
  this->setSuperblock(block);
}

void UFS::eraseFile(uint32_t inode_index)
{
  Inode inode = getInode(inode_index);
  if (inode.size == 0)
    return;

  uint32_t block_pts = 0;
  uint32_t bytes_freed = 0;

  while(bytes_freed < inode.size)
  {
    if(block_pts >= 0 && block_pts <= 9)
    {
      this->freeMemoryBlock(inode.data[block_pts]);
      block_pts++;
      bytes_freed += this->device->BLOCK_SIZE;
    }
    else if(block_pts == 10)
    {
      uint8_t data[4096];
      this->device->readBlock(inode.data[10], data);
      uint32_t i = 0;
      while(i < 1024 && bytes_freed < inode.size)
      {
        uint32_t *block_index = (uint32_t*)(data + i*4);
        this->freeMemoryBlock(*block_index);
        bytes_freed += this->device->BLOCK_SIZE;
        i++;
      }
      this->freeMemoryBlock(inode.data[10]);
    }
  }
  inode.size = 0;
  this->setInode(inode_index, inode);
}

void UFS::writeToFile(uint32_t inode_index, uint8_t *contents, uint32_t size)
{
  Inode inode = this->getInode(inode_index);
  if(inode.size != 0)
    this->eraseFile(inode_index);

  uint32_t bytes_wrote = 0;
  uint8_t block_pts = 0;
  while(bytes_wrote != size)
  {
    if((block_pts >= 0) && (block_pts <= 9))
    {
      uint8_t tmp[4096];
      uint32_t freed_block = this->requestMemoryBlock();
      uint32_t i = 0;
      while(bytes_wrote < size && i < this->device->BLOCK_SIZE)
      {
        tmp[i] = contents[bytes_wrote];
        i++;
        bytes_wrote++;
      }
      this->device->writeBlock(freed_block, tmp);
      inode.data[block_pts] = freed_block;
    }
    else if(block_pts == 10)
    {
      uint32_t single_indirect_block = this->requestMemoryBlock();
      inode.data[10] = single_indirect_block;
      uint8_t data[4096];
      uint32_t i = 0;
      while(i < 1024 && bytes_wrote != size)
      {
        uint32_t new_block = this->requestMemoryBlock();
        uint8_t block_data[4096];
        uint32_t j = 0;
        while(j < 4096 && bytes_wrote != size)
        {
          block_data[j] = contents[bytes_wrote];
          j++;
          bytes_wrote++;
        }
        uint32_t *address = (uint32_t*)(data + i*4);
        *address = new_block;
        this->device->writeBlock(new_block, block_data);
        i++;
      }
      this->device->writeBlock(single_indirect_block, data);
    }
    block_pts++;
  }
  inode.size = size;
  this->setInode(inode_index, inode);
}


void UFS::readFromFile(uint32_t inode_index, uint8_t *contents)
{
  Inode inode = this->getInode(inode_index);
  uint32_t bytes_read = 0;
  uint8_t block_pts = 0;
  while(bytes_read != inode.size)
  {
    if((block_pts >= 0) && (block_pts <= 9))
    {
      uint32_t block_index = inode.data[block_pts];
      uint8_t data[4096];
      this->device->readBlock(block_index, data);
      uint32_t i = 0;
      while(i < this->device->BLOCK_SIZE && bytes_read != inode.size)
      {
        contents[bytes_read] = data[i];
        bytes_read++;
        i++;
      }
    }
    else if(block_pts == 10)
    {
      uint32_t single_indirect_block = inode.data[10];
      uint8_t indirect_block[4096];
      this->device->readBlock(single_indirect_block, indirect_block);
      uint32_t i = 0;
      while(i < 1024 && bytes_read != inode.size)
      {
        uint32_t *ptr = (uint32_t*)(indirect_block + i*4);
        uint8_t block_data[4096];
        this->device->readBlock(*ptr, block_data);
        uint32_t j = 0;
        while(j < this->device->BLOCK_SIZE && bytes_read != inode.size)
        {
          contents[bytes_read] = block_data[j];
          bytes_read++;
          j++;
        }
        i++;
      }
    }
    block_pts++;
  }
}

Directory* UFS::getRoot()
{
  Inode inode = this->getInode(0);
  return new Directory("root", nullptr, 0, 0);
}

uint32_t UFS::file_list_to_string(std::vector<Entity*> entities, uint8_t **location)
{
  uint32_t no_of_entites = entities.size();
  // allocate 12 bytes for name and 4 bytes for inode index
  uint8_t *mem = new uint8_t[no_of_entites*16 + 4];
  uint32_t *no = (uint32_t*)mem;
  *no = no_of_entites;
  for(int i = 0; i < no_of_entites; i++)
  {
    char* file_name_loc = (char*)(mem + 4 + i*16);
    strcpy(file_name_loc, entities[i]->getName().c_str());
    uint32_t *inode_index = (uint32_t*)(mem + 4 + i*16 + 12);
    *inode_index = entities[i]->getInodeIndex();
  }
  *location = mem;
  return no_of_entites*16 + 4;
}

uint32_t UFS::getFilesCountOfInode(uint32_t inode_index)
{
  Inode inode = this->getInode(inode_index);
  if(inode.type != 1)
    return 0;
  uint32_t size = inode.size;
  size -= 4;
  return size / 16;
}

void UFS::string_to_file_list(uint8_t *loc, Directory* parent, std::vector<Entity*> &entities)
{
  uint32_t *size = (uint32_t*)(loc);
  for(int i = 0; i < *size; i++)
  {
    std::string name((char*)(loc + 4 + 16*i));
    uint32_t inode_number = *(uint32_t*)(loc + 4 + 16*i + 12);
    Inode inode = this->getInode(inode_number);
    Entity *entity;
    if(inode.type == 0)
      entity = (Entity*)new File(name, parent, inode.size, inode_number);
    else
      entity = (Entity*)new Directory(name, parent, this->getFilesCountOfInode(inode_number), inode_number);
    entities.push_back(entity);
  }
}


File* UFS::createFile(Entity* parent, std::string name)
{
  Inode inode = this->getInode(parent->getInodeIndex());
  if(inode.size == 0)
  {
    uint32_t new_file_inode = this->requestInode();
    Inode new_inode;
    new_inode.type = 0;
    new_inode.size = 0;
    this->setInode(new_file_inode, new_inode);

    std::vector<Entity*> entities;
    Entity* entity = (Entity*)new File(name, (Directory*)parent, 0, new_file_inode);
    entities.push_back(entity);

    uint8_t *loc;
    uint32_t size_string = this->file_list_to_string(entities, &loc);

    writeToFile(parent->getInodeIndex(), loc, size_string);
    return (File*)entity;
  }
  else
  {
    uint32_t new_file_inode = this->requestInode();
    Inode new_inode;
    new_inode.type = 0;
    new_inode.size = 0;
    this->setInode(new_file_inode, new_inode);

    uint32_t size_parent_inode = inode.size;
    uint8_t *arr = new uint8_t[size_parent_inode];
    readFromFile(parent->getInodeIndex(), arr);
    std::vector<Entity*> entities;
    string_to_file_list(arr, (Directory*)parent, entities);
    Entity* entity = (Entity*)new File(name, (Directory*)parent, 0, new_file_inode);
    entities.push_back(entity);
    delete[] arr;
    uint8_t *loc;
    uint32_t size_string = this->file_list_to_string(entities, &loc);
    writeToFile(parent->getInodeIndex(), loc, size_string);
    return (File*)entity;
  }
}

Directory* UFS::createDirectory(Entity* parent, std::string name)
{
  Inode inode = this->getInode(parent->getInodeIndex());
  if(inode.size == 0)
  {
    uint32_t new_file_inode = this->requestInode();
    Inode new_inode;
    new_inode.type = 1;
    new_inode.size = 0;
    this->setInode(new_file_inode, new_inode);

    std::vector<Entity*> entities;
    Entity* entity = (Entity*)new Directory(name, (Directory*)parent, 0, new_file_inode);
    entities.push_back(entity);

    uint8_t *loc;
    uint32_t size_string = this->file_list_to_string(entities, &loc);

    writeToFile(parent->getInodeIndex(), loc, size_string);
    return (Directory*)entity;
  }
  else
  {
    uint32_t new_file_inode = this->requestInode();
    Inode new_inode;
    new_inode.type = 1;
    new_inode.size = 0;
    this->setInode(new_file_inode, new_inode);

    uint32_t size_parent_inode = inode.size;
    uint8_t *arr = new uint8_t[size_parent_inode];
    readFromFile(parent->getInodeIndex(), arr);
    std::vector<Entity*> entities;
    string_to_file_list(arr, (Directory*)parent, entities);
    Entity* entity = (Entity*)new Directory(name, (Directory*)parent, 0, new_file_inode);
    entities.push_back(entity);
    delete[] arr;
    uint8_t *loc;
    uint32_t size_string = this->file_list_to_string(entities, &loc);
    writeToFile(parent->getInodeIndex(), loc, size_string);
    return (Directory*)entity;
  }
}


void UFS::dir(Directory *parent, std::vector<Entity*> &entities)
{
  uint32_t inode_index = parent->getInodeIndex();
  Inode inode = this->getInode(inode_index);

  uint32_t size = inode.size;
  uint8_t *arr = new uint8_t[size];
  readFromFile(inode_index, arr);
  string_to_file_list(arr, (Directory*)parent, entities);
  delete[] arr;
}

void UFS::writeFile(File* file, uint32_t size, uint8_t *bytes)
{
  uint32_t inode_index = file->getInodeIndex();
  writeToFile(inode_index, bytes, size);
  file->setSize(size);
}

void UFS::readFile(File* file, uint8_t *bytes)
{
  uint32_t inode_index = file->getInodeIndex();
  readFromFile(inode_index, bytes);
}

void UFS::deleteFile(File* file)
{
  Directory *parent = file->getParent();
  uint32_t inode_index = file->getInodeIndex();
  this->eraseFile(inode_index);
  uint32_t parent_inode = parent->getInodeIndex();
  std::vector<Entity*> entities;
  Inode parent_inode_a = this->getInode(parent_inode);
  uint8_t *blob = new uint8_t[parent_inode_a.size];
  readFromFile(parent_inode, blob);
  string_to_file_list(blob, parent, entities);
  delete[] blob;
  int to_delete = -1;
  for(int i = 0; i < entities.size(); i++)
  {
    if(entities[i]->getInodeIndex() == inode_index)
    {
      to_delete = i;
    }
  }
  if(to_delete != -1)
  {
    entities.erase(entities.begin() + to_delete);
  }
  this->freeInode(inode_index);
  uint32_t size_string = file_list_to_string(entities, &blob);
  this->writeToFile(parent_inode, blob, size_string);
}


void UFS::printFile(int tab, File* file)
{
  for(int i = 0; i < tab; i++)
  {
    std::cout << "|--";
  }
  std::cout << file->getName() << std::endl;
}

void UFS::printDirectory(int tab, Directory *root)
{
  for(int i = 0; i < tab; i++)
  {
    std::cout << "|--";
  }
  std::cout << root->getName() << std::endl;
  std::vector<Entity*> entities;
  this->dir(root, entities);
  for(int i = 0; i <  entities.size(); i++)
  {
    Entity* entity = entities[i];
    if(entity->getType() == Entity::Type::File)
      this->printFile(tab + 1, (File*)entity);
    else if(entity->getType() == Entity::Type::Dir)
      this->printDirectory(tab + 1, (Directory*)entity);
  }
}
